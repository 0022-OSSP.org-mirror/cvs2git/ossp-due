##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.term.sh: DUE module for terminal configuration
##

#   require DUE "platform" module
due load platform

#   determine terminal type
if [ -z TERM ]; then
    TERM=vt100
fi
TERM_COLOR=$TERM
if [ ".$TERM" = .xterm ]; then
    if [ ".$OSNAME" = .freebsd -o ".$OSNAME" = .linux ]; then
        TERM_COLOR=xterm-color
    fi
fi

#   SLang default color settings
export COLORFGBG="default;default"

#   adjust terminal settings
if expr $- : ".*i.*" >/dev/null; then
    BASH_INTERACTIVE=yes
else
    BASH_INTERACTIVE=no
fi
if [ ".$BASH_INTERACTIVE" = .yes ]; then
    mesg y
fi  
if [ ".$TERM" = .xterm -a ".$BASH_INTERACTIVE" = .yes ]; then
    stty -istrip cs8
fi
shopt -s checkwinsize

#   provide command for terminal reset
alias sane='(xset r rate 400 40; stty sane; eval `resize`) >/dev/null 2>&1 || true'

#   adjust shell history settings
PROMPT_COMMAND='builtin history -a'
shopt -s histappend
shopt -s cmdhist
HISTSIZE=9000
HISTFILESIZE=9000
HISTIGNORE="&"
HISTTIMEFORMAT='%Y-%m-%d %H:%M:%S  '
bind '"\e[A"':history-search-backward
bind '"\e[B"':history-search-forward

#   provide command for rotating a little propeller for
#   keeping a remote connection busy and this way alive
function zz () {
    echo -n "Sleeping... "
    while [ 1 ]; do
        for i in "|" "/" "-" "\\"; do
            echo -n "$i"
            sleep 1
            echo -ne "\b"
        done
    done
}

#   login/logout tracing
if [ $SHLVL -eq 0 -a ".$BASH_INTERACTIVE" = .yes ]; then
    #   shell history annotation
    echo "#[`date '+%Y-%m-%d/%H:%M'`] login by user ${SSHUSER:-unknown}" >>$HISTFILE

    #   interactive messages
    echo "$HOSTNAME: user $USER${SSHUSER:+ (${SSHUSER:-unknown})}, login at `date`"
    trap 'echo "$HOSTNAME: user $USER${SSHUSER:+ (${SSHUSER:-unknown})}, logout at `date`"' 0
fi

