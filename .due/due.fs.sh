##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.fs.sh: DUE module for filesystem settings
##

#   set umask(2) for creating files not group/other writable
umask 022

#   shell command for quickly finding files
function ff () {
    if [ $? -eq 0 ]; then
        echo "Usage: ff <file-glob> <directory>" 1>&2
        return 1
    fi
    local file=$1; shift
    local dir=$2;  shift
    find "$dir" -name "*${file}*" -type f -print
    return 0
}

#   canonicalization of df(1) output
alias df='command df -k'

#   convert between Unix/LF and Windows/CRLF
if type -P recode 2>/dev/null; then
    #   full charset conversion
    alias unix2win="recode ISO-8859-1..CP1252/CR-LF"
    alias win2unix="recode CP1252/CR-LF..ISO-8859-1"
elif type -P perl 2>/dev/null; then
    #   sufficient for at least CR/LF conversion
    alias unix2win="perl -p -i.orig -e 's/\r?\n/\r\n/sg;'"
    alias win2unix="perl -p -i.orig -e 's/\r\n/\n/sg;'"
fi

