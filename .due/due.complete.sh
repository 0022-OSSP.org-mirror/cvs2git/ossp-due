##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.complete.sh: DUE module for command completion
##

#   turn on extended globbing
shopt -s extglob no_empty_cmd_completion

#   make directory commands see only directories
complete -d cd mkdir rmdir pushd

#   make file commands see only (their) files
complete -f cat less more ln strip
complete -f -X '*.bz2' bzip2 
complete -f -X '!*.bz2' bunzip2
complete -f -X '!*.zip' unzip
complete -f -X '*.gz' gzip
complete -f -X '*.Z' compress
complete -f -X '!*.+(Z|gz|tgz|Gz)' gunzip zcat zmore
complete -f -X '!*.Z' uncompress zmore zcat
complete -f -X '!*.+(gif|jpg|jpeg|GIF|JPG|bmp)' xv display
complete -f -X '!*.+(ps|PS|ps.gz)' gv gs mgv
complete -f -X '!*.+(dvi|DVI)' dvips xdvi
complete -f -X '!*.+(pdf|PDF)' acroread xpdf
complete -f -X '!*.texi*' makeinfo texi2dvi texi2html
complete -f -X '!*.+(tex|TEX)' tex latex
complete -f -X '!*.+(c|cc|C|h|hh|H)' cc gcc

#   make signal and job commands see only signals/jobs
complete -A signal kill -P '%'
complete -A stopped -P '%' bg
complete -j -P '%' fg jobs 

#   make user/group commands see only users/groups
complete -u passwd su finger

#   make network commands see only hostnames
complete -A hostname ssh rsh telnet rlogin ftp ping host traceroute nslookup dig

#   make internal commands see only their possibilities
complete -v export local readonly unset
complete -A setopt set
complete -A shopt shopt
complete -A helptopic help
complete -a unalias
complete -A binding bind

