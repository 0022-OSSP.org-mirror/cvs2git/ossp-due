##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.platform.sh: DUE module for platform specifics
##

#   requires "util" module
due load util

#   determine name of operating system
OSNAME=`echo "$OSTYPE" | sed -e 's;^\([a-zA-Z][a-zA-Z]*\).*$;\1;'`
OSVEND=unknown

#   adjust platform specific base run-time environment
case $OSNAME in
    freebsd )
        PATH="/bin:/usr/bin:/sbin:/usr/sbin"
        LD_LIBRARY_PATH="/usr/lib:/usr/lib/compat"
        MANPATH="/usr/share/man"
        ;;
    linux )
        PATH="/bin:/usr/bin:/sbin:/usr/sbin"
        LD_LIBRARY_PATH="/lib:/usr/lib"
        MANPATH="/usr/man:/usr/share/man"
        if   [ -f /etc/fedora-release ]; then OSVEND=Fedora
        elif [ -f /etc/redhat-release ]; then OSVEND=RedHat
        elif [ -f /etc/debian_version ]; then OSVEND=Debian
        elif [ -f /etc/SuSE-release   ]; then OSVEND=SuSE
        fi
        ;;
    solaris ) 
        PATH="/usr/bin:/usr/ccs/bin:/usr/sbin:/sbin"
        LD_LIBRARY_PATH="/usr/lib:/usr/ccs/lib"
        MANPATH="/usr/share/man"
        ;;
    * )
        PATH=`find_path : /bin /usr/bin /sbin /usr/sbin /usr/ccs/bin`
        LD_LIBRARY_PATH=`find_path : /lib /usr/lib /usr/ccs/lib`
        MANPATH=`find_path : /usr/man /usr/share/man`
        ;;
esac
activate_generic /usr/X11R6 /usr/X11 /usr/openwin

#   platform specific convenience command line argument mapping
case $OSNAME in
    solaris | hpux )
        function ps () { 
            if [ ".$1" = ".-ax" -o ".$1" = ".-aux" ]; then
                shift
                command ps -ef "$@"
            else
                command ps "$@"
            fi
        }
        ;;
esac    

