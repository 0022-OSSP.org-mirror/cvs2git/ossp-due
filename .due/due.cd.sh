##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.cd.sh: DUE module for enhanced change directory command
##

#   hook into cd command to adjust environment
function cd () {
    #   change current working directory
    if [ ".$1" = .- ]; then
        #   go to previous working directory on forward directory stack
        #   and move this directory onto the reverse directory stack
        if [ ${#DIRSTACK[*]} -gt 1 ]; then
            DIRSTACKREV[${#DIRSTACKREV[*]}]="${DIRSTACK[0]}"
            builtin popd >/dev/null
        else
            echo "cd: no more previous working directories on forward directory stack" 1>&2
            return 1
        fi
    elif [ ".$1" = .+ ]; then
        #   go to previous working directory on reverse directory stack
        #   and move this directory onto the forward directory stack
        if [ ${#DIRSTACKREV[*]} -gt 0 ]; then
            local i=$((${#DIRSTACKREV[*]} - 1))
            eval "builtin pushd ${DIRSTACKREV[$i]} >/dev/null"
            unset DIRSTACKREV[$i]
        else
            echo "cd: no more previous working directories on reverse directory stack" 1>&2
            return 1
        fi
    else
        #   go to next working directory
        #   and move this directory onto the forward directory stack
        if [ $# -eq 0 ]; then
            set -- $HOME
        fi
        pushd ${1+"$@"} >/dev/null
        if [ "${#DIRSTACK[*]}" -ge 2 -a "${DIRSTACK[0]}" = "${DIRSTACK[1]}" ]; then
            builtin popd >/dev/null
        fi
        DIRSTACKREV=()
    fi

    if [ ".$BASH_INTERACTIVE" = .yes ]; then
        #   adjust the command-line prompt with new $PWD (always)
        PS1="\\u@\\h:$PWD\n\\\$ "

        #   adjust the X11 terminal window title with new $PWD (optional)
        case "$TERM" in
            xterm | xterm-* )
                echo -n $'\e]0;'"$USER@$HOSTNAME:$PWD"$'\cg'
                ;;
        esac
    fi
}

#   initially adjust the environment
cd $PWD
PS2="> "

#   provide convenience shortcuts
alias -- +='cd +'
alias -- -='cd -'
alias -- ..='cd ..'

#   auto-correct small errors in given directory names
shopt -s cdspell

