##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.rescue.sh: DUE module for rescue command
##

#   system rescue facility
function rescue () {
    local cmd="$1"
    shift
    case "$cmd" in
        on )
            rescue_time="${1:-10}"
            echo "++ scheduling rescue through system reboot in ${rescue_time} minutes"
            case $OSTYPE in
                FreeBSD* )
                    echo "/sbin/shutdown -r now" | at -q r +${rescue_time} minutes >/dev/null
                    ;;
                Linux* )
                    echo "/sbin/shutdown -r now" | at -q r +${rescue_time} minutes >/dev/null
                    ;;
                SunOS* )
                    echo "/usr/sbin/shutdown -i6 -g0 -y" | at -q r now "+ ${rescue_time}minutes" >/dev/null
                    ;;
            esac
            ;;
        off )
            echo "++ cancelling rescue through system reboot"
            case $OSTYPE in
                FreeBSD* )
                    for id in `at -q r -l | grep -v "^Date" | awk '{ print $5; }'`; do
                        at -q r -d $id >/dev/null
                    done
                    ;;
                Linux* )
                    for id in `at -q r -l | awk '{ print $1; }'`; do
                        at -q r -d $id >/dev/null
                    done
                    ;;
                SunOS* )
                    for id in `at -q r -l | awk '{ print $1; }'`; do
                        at -r $id >/dev/null
                    done
                    ;;
            esac
            unset rescue_time
            ;;
        renew|new )
            echo "++ rescheduling rescue through system reboot in ${rescue_time} minutes"
            x=${rescue_time}
            rescue off >/dev/null || true
            rescue on ${x} >/dev/null
            ;;
        * )
            echo "Usage: rescue on [<minutes>]" 2>&1
            echo "Usage: rescue [re]new" 2>&1
            echo "Usage: rescue off" 2>&1
            ;;
    esac
}

