##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.ls.sh: DUE module for enhanced list directory command
##

#   ls command which enables color sequences and automatic paging
#   if the output is connected to the interactive terminal.
function ls () { 
    local prolog=""
    local cmd="command ls"
    local opt=""
    local arg="\${1+\"\$@\"}"
    local epilog=""
    if [ -t /dev/stdout ]; then
        if [ ".$TERM" != ".$TERM_COLOR" ]; then
            prolog="TERM=\$TERM_COLOR $prolog"
        fi
        if [ ".$OSNAME" = .freebsd ]; then
            prolog="$prolog CLICOLOR=1 CLICOLOR_FORCE=1"
            opt="-C"
            epilog="| /usr/bin/less -E -r"
        elif [ ".$OSNAME" = .linux ]; then
            opt="--color=always"
            epilog="| /usr/bin/less -X -E -r -s | grep -v '^.\\[m'"
        elif [ ".$OSNAME" = .solaris -a -f /bin/less ]; then
            epilog="| /bin/less -E -r"
        elif [ ".$PAGER" = .less ]; then
            epilog="| less -E -r"
        fi
    fi
    eval "$prolog $cmd $opt $arg $epilog"
}

#   some convenience commands
alias ll="ls -l"
alias lx="ls -l -a -o"

