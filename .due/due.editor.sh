##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.editor.sh: DUE module for smart editor/pager handling
##

#   requires "util" module
due load util

#   determine best editor
export EDITOR
EDITOR=`find_tool vim vi`
if [ ".$EDITOR" = .vim ]; then
    alias vi=vim
    alias pi='vim "+set patchmode=.orig"'
fi
if [ ".$TERM" != ".$TERM_COLOR" ]; then
    eval "function $EDITOR () { TERM=\$TERM_COLOR command $EDITOR \${1+\"\$@\"}; }"
fi

#   determine best pager
export PAGER
PAGER=`find_tool less more`
if [ ".$PAGER" = .less ]; then
    PAGER="$PAGER -E -r"
    alias more="$PAGER"
    export LESSOPEN
    LESSOPEN=`find_tool lesspipe`
    if [ -n "$LESSOPEN" ]; then
        LESSOPEN="|$LESSOPEN %s"
    else
        unset LESSOPEN
    fi
fi

