##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.tmpdir.sh: DUE module for TMPDIR handling 
##

if [ -z $TMPDIR ]; then
    if [ -d $HOME/tmp ]; then
        #   reuse own temporary directory
        TMPDIR=$HOME/tmp
        export TMPDIR
    elif [ -d /tmp ]; then
        if [ ! -d /tmp/$LOGNAME ]; then
            #   just try to create it without checking results
            (umask 066; mkdir /tmp/$LOGNAME) >/dev/null 2>&1 || true
        fi
        if [ -d /tmp/$LOGNAME ]; then
            #   check whether the existing directory is really owned by
            #   us and at the same time fix possible wrong permissions
            #   (from an previously existing directory)
            chmod 711 /tmp/$LOGNAME >/dev/null 2>&1
            if [ $? -eq 0 ]; then
                TMPDIR=/tmp/$LOGNAME
                export TMPDIR
            fi
        fi
    fi    
fi

