##
##  OSSP due - Dynamic User Environment
##  Copyright (c) 1994-2004 Ralf S. Engelschall <rse@engelschall.com>
##  Copyright (c) 1994-2004 The OSSP Project <http://www.ossp.org/>
##
##  This file is part of OSSP due, a dynamic user environment
##  which can found at http://www.ossp.org/pkg/tool/due/
##
##  Permission to use, copy, modify, and distribute this software for
##  any purpose with or without fee is hereby granted, provided that
##  the above copyright notice and this permission notice appear in all
##  copies.
##
##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
##  SUCH DAMAGE.
##
##  due.util.sh: DUE module for shared utility functions
##

function lookup () {
    #   options defaults
    local opt_d=no      # search for directory only
    local opt_f=no      # search for file only
    local opt_a=no      # return all occurrences instead of just first one
    local opt_b=no      # return basename of files only
    local opt_s=" "     # separator string when returning all occurrences
    local opt_p=${PATH} # directory path to search
    local opt_r=no      # recursive search inside paths

    #   parse command-line options
    local opt
    OPTIND=1
    while getopts dfas:p:r opt; do
        case ${opt} in
            d ) opt_d=yes     ;;
            f ) opt_f=yes     ;;
            a ) opt_a=yes     ;;
            s ) opt_s=$OPTARG ;;
            p ) opt_p=$OPTARG ;;
            r ) opt_r=yes     ;;
        esac
    done
    shift $(($OPTIND - 1))

    #   for all filenames...
    local result=""
    local file
    for file in "$@"; do
        #   ...iterate over all directories in the path...
        local topdir
        for topdir in ${opt_p//:/ }; do
            if [ ! -d ${topdir} ]; then
                continue
            fi
            if [ ${opt_r} = yes ]; then
                topdir=`find $topdir -type d -print`
            fi
            #   ...and optionally all its sub-directories...
            local dir
            local ifso="${IFS}"; IFS="\n"
            for dir in ${topdir}; do
                IFS="${ifso}"
                local flag="-e"
                if   [ ${opt_d} = yes ]; then
                    flag="-d"
                elif [ ${opt_f} = yes ]; then
                    flag="-f"
                fi
                if [ ${flag} ${dir}/${file} ]; then
                    if [ ${opt_b} = yes ]; then
                        result="${result}${result:+${opt_s}}${file}"
                    else
                        result="${result}${result:+${opt_s}}${dir}/${file}"
                    fi
                    if [ ${opt_a} = no ]; then
                        break
                    fi
                fi
            done
            IFS="${ifso}"
            if [ -n ${result} ]; then
                break
            fi
        done
        if [ -n ${result} ]; then
            break
        fi
    done
    return $result
}

#   assemble existing directories
function find_path () {
    local sep="$1"
    shift
    local path=""
    local dir
    for dir in "$@"; do
        if [ -d $dir ]; then
            path="${path}${path:+$sep}${dir}"
        fi
    done
    echo "$path"
}

#   lookup preferred tool in $PATH
function find_tool () {
    local tool=""
    local name
    for name in "$@"; do
        if [ ".`type -p $name`" != . ]; then
            tool="$name"
            break
        fi
    done
    echo "$tool"
}

#   locate and activate OpenPKG software instances
function activate_openpkg () {
    local prefix
    for prefix in "$@"; do
        if [ -d $prefix/etc/openpkg ]; then
            PATH="$PATH:$prefix/bin:$prefix/local/bin:$prefix/sbin:$prefix/local/sbin"
            LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$prefix/lib"
            MANPATH="$MANPATH:$prefix/man:$prefix/local/man"
        fi
    done
}

#   locate and activate generic software instance
function activate_generic () {
    local prefix
    for prefix in "$@"; do
        if [ -d $prefix ]; then
            if [ -d $prefix/bin ]; then
                PATH="$PATH:$prefix/bin"
            fi
            if [ -d $prefix/lib ]; then
                LD_LIBRARY_PATH="$LD_LIBRARY_PATH:$prefix/lib"
            fi
            if [ -d $prefix/man ]; then
                MANPATH="$MANPATH:$prefix/man"
            fi
        fi
    done
}

#   repeat a command
function repeat () {
    if [ $# -eq 0 ]; then
        echo "Usage: repeat <number>|oo <command> [...]" 1>&2
        return 1
    fi
    count="$1"
    shift
    if [ ".$count" = .oo ]; then
        while true; do
            eval "$@" || exit $?
        done
    else
        local i=0
        while [ $i -lt $count ]; do
            eval "$@" || exit $?
            i=$(($i + 1))
        done
    fi
}

