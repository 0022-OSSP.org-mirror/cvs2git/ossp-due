"##
"##  OSSP due - Dynamic User Environment
"##  Copyright (c) 1994-2005 Ralf S. Engelschall <rse@engelschall.com>
"##  Copyright (c) 1994-2005 The OSSP Project <http://www.ossp.org/>
"##
"##  This file is part of OSSP due, a dynamic user environment
"##  which can found at http://www.ossp.org/pkg/tool/due/
"##
"##  Permission to use, copy, modify, and distribute this software for
"##  any purpose with or without fee is hereby granted, provided that
"##  the above copyright notice and this permission notice appear in all
"##  copies.
"##
"##  THIS SOFTWARE IS PROVIDED ``AS IS'' AND ANY EXPRESSED OR IMPLIED
"##  WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF
"##  MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED.
"##  IN NO EVENT SHALL THE AUTHORS AND COPYRIGHT HOLDERS AND THEIR
"##  CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL,
"##  SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT
"##  LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF
"##  USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND
"##  ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
"##  OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT
"##  OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF
"##  SUCH DAMAGE.
"##
"##  ~/.vimrc: Vi-IMproved (VIM) configuration
"##  vi: set tabstop=4 shiftwidth=4:
"##

version 6.0

"   Remove ALL autocommands.
:autocmd! 

"   Always insert tabs as spaces (only exception are Makefiles) 
"   but preserve already existing tabs in order to not destroy existing content.
set expandtab
autocmd BufRead Makefile,Makefile.* set noexpandtab

"   Display 8-bit characters
set graphic

"   Avoid "Thanks for Flying Vim" titles in xterm and
"   set the X11 icon to the name of Vim instead of xterm.
set notitle
set icon

"   Check first 6 lines for Vi set commands (e.g. 'vi:set tabstop=4 shiftwidth=4').
set modeline
set modelines=6

"   No automatic write of files, i.e. I KNOW WHAT I DO! At least most of the time ;-)
set noautowrite

"   Always use my login shell (GNU Bash) for sub-shells.
set shell=$SHELL

"   Create .bak files only while saving. If current dir is not
"   allowed to create the file, save it under the backup dir.
set nobackup
set writebackup
set backupdir=$HOME/tmp

"   Write swap files (*.swp) to this dir if not allowed at the current one.
set directory=$HOME/tmp

"   Don't beep for every silly command and write shorter messages.
set noerrorbells
set terse

"   Read/write a .viminfo file, but don't store more than 50 lines of registers.
set viminfo='20,\"50

"   Show command and modes.
set showcmd
set showmode

"   Display a status line plus a single-line command line
"   and always tell me where I'm inside the file.
set laststatus=2
set cmdheight=1
set ruler

"   Do incremental search with smart case sensitivity behavior.
set incsearch
set smartcase

"   Allow virtual editing in Visual block mode.
set virtualedit=block

"   Always expand windows to the maximum height, but expand only to the
"   minimum width of 100 characters per line. Also, create new windows
"   below current one.
set winheight=9999
set winwidth=100
set splitbelow

"   Do not wrap lines (neither implicitly on display nor explicitly
"   during editing), except for real text files. Also, shift a minimum
"   of 4 chars left/right if we go over the right/left border.
set textwidth=9999
autocmd BufRead *.txt,*.tex,*.xml set textwidth=78
set nowrap
set shiftwidth=4

"   Always indent new lines the same as the last one.
"   Also, use a C-programmer-style smart tab editing.
set autoindent
set smartindent
set tabstop=4

"	Make Y analogous to C and D for orthogonality
map Y y$

"   Allow us to backspace back over lines in insert mode and also
"   to go back over the starting point.
set backspace=2

"   Command line uses more Bash style editing
cnoremap <C-A>      <Home>
cnoremap <C-F>      <Right>
cnoremap <C-B>      <Left>
cnoremap <ESC>b     <S-Left>
cnoremap <ESC>f     <S-Right>
cnoremap <ESC><C-H> <C-W>

"   Vim and other tools inside an xterm think Delete key is <backspace> 
"   and Backspace key is <delete> but we cannot just swap them. In
"   Vim this swapping doesn't work and via xmodmap other X11 applications
"   would be affected, too. So we the following: We map Delete to CTRL-D
"   for the XTerms via xrdb and then remap the keys here in Vim.
imap  <Esc>lxi
imap  <Esc>OD

"   Make qualified cursor keys the same as non-qualfied cursor keys
"   in order to avoid typing problems in case I'm lazy releasing the
"   qualifying keys (shift, control, alt).
map  <Esc>[1;2A <Esc>OA 
map  <Esc>[1;2B <Esc>OB 
map  <Esc>[1;2C <Esc>OC 
map  <Esc>[1;2D <Esc>OD 
map  <Esc>[1;3A <Esc>OA 
map  <Esc>[1;3B <Esc>OB 
map  <Esc>[1;3C <Esc>OC 
map  <Esc>[1;3D <Esc>OD 
map  <Esc>[1;5A <Esc>OA 
map  <Esc>[1;5B <Esc>OB 
map  <Esc>[1;5C <Esc>OC 
map  <Esc>[1;5D <Esc>OD 
imap <Esc>[1;2A <Esc>OA
imap <Esc>[1;2B <Esc>OB
imap <Esc>[1;2C <Esc>OC
imap <Esc>[1;2D <Esc>OD
imap <Esc>[1;3A <Esc>OA
imap <Esc>[1;3B <Esc>OB
imap <Esc>[1;3C <Esc>OC
imap <Esc>[1;3D <Esc>OD
imap <Esc>[1;5A <Esc>OA
imap <Esc>[1;5B <Esc>OB
imap <Esc>[1;5C <Esc>OC
imap <Esc>[1;5D <Esc>OD

"   Provide CTRL-G as an alias for CTRL-] (the jump forward key) because
"   on a german keyboard CTRL-] is horrible to type, it is a free
"   keystroke, it is located closer to CTRL-T (the jump back key) and
"   "G" can be more easily remembered by thinking "go to".
map <C-G> <C-]>

